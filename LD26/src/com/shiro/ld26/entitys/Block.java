package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Block extends Entity
{

	public Block(int x, int y, int width, int height)
	{
		super(x, y, width, height);
		type = "block";
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
        g.setColor(Color.WHITE);
        g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
        g.fillRect(0, 0, width, height);
        g.setTransform(tx);
        super.render(g, d);
	}

}
