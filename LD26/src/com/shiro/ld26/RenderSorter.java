package com.shiro.ld26;

import java.util.Comparator;

import com.shiro.ld26.entitys.Entity;

public class RenderSorter implements Comparator<Entity>
{
	@Override
	public int compare(Entity e1, Entity e2)
	{
		return e2.renderOrder - e1.renderOrder;
	}

}
