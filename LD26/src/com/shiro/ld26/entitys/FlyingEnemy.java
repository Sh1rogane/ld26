package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import com.shiro.ld26.Game;
import com.shiro.ld26.Sound;

public class FlyingEnemy extends Mob
{

	private int hurtTick = 0;
	public FlyingEnemy(int x, int y, int width, int height)
	{
		super(x, y, width, height);
		speed = 3;
		type = "enemy";
		hp = 3;
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
		g.setColor(Color.BLUE);
		if(hurt)
        	g.setColor(Color.WHITE);
		g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
		g.fillRect(0, 0, width, height);
		g.setTransform(tx);
		super.render(g, d);
	}
	@Override
	public void update()
	{
		super.update();

		if(!dead)
		{
			if(hurt)
			{
				hurtTick ++;
				if(hurtTick > 5)
					hurt = false;
			}
			
			Player p = Game.currentScreen.getPlayer();

			if(p.x > x)
				vx = speed;
			else if(p.x < x)
				vx = -speed;
			if(p.y > y)
				vy = speed;
			else if(p.y < y)
				vy = -speed;
			
			Entity e = collides("bullet");
			if(e != null)
			{
				solid = false;
				hurt = true;
				Sound.enemyHit.play();
				hp -= e.damage;
				e.remove();
			}
			if(hp <= 0)
			{
				dead = true;
				vx *= -1;
				vy = -15;
			}
		}
		else
		{
			vy += 2;

			if(y > Game.GAME_HEIGHT + 100)
				removeSelf();
		}
	}
}
