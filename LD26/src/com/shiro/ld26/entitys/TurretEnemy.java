package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import com.shiro.ld26.Game;
import com.shiro.ld26.Sound;

public class TurretEnemy extends Mob
{
	private double rotation = 0;
	private int shootTick = 0;
	private int hurtTick = 0;
	
	public TurretEnemy(int x, int y, int dir)
	{
		super(x, y, 60, 60);
		this.dir = dir;
		hp = 15;
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
        g.setColor(Color.CYAN);
        if(hurt)
        	g.setColor(Color.WHITE);
        g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
        if(dir == Mob.UP)
        	g.fillArc(0, 0, width, height, 0, 180);
        if(dir == Mob.DOWN)
        	g.fillArc(0, 0, width, height, 180, 180);
        if(dir == Mob.RIGHT)
        	g.fillArc(0, 0, width, height, 90, 180);
        if(dir == Mob.LEFT)
        	g.fillArc(0, 0, width, height, 270, 180);
        
        g.rotate(Math.toRadians(rotation), 30, 30);
        g.fillRect(width / 2 - 5, -18, 10, 20);
        g.setTransform(tx);
        super.render(g, d);
	}
	@Override
	public void update()
	{
		super.update();
		if(!dead)
		{
			if(hurt)
			{
				hurtTick++;
				if(hurtTick > 5)
					hurt = false;
			}
			
			Player p = Game.currentScreen.getPlayer();
			rotation = Math.toDegrees(Math.atan2(p.y - y, p.x - x)) + 90;
			shootTick++;
			if(shootTick >= 30)
			{
				Sound.pShoot.play();
				shootTick = 0;
				double bvx = Math.cos(Math.toRadians(rotation - 90)) * 10;
				double bvy = Math.sin(Math.toRadians(rotation - 90)) * 10;
				Game.currentScreen.add(new TurretBullet(x + 25, y + 18, bvx, bvy));
			}
			Entity e = collides("bullet");
			if(e != null)
			{
				hurt = true;
				Sound.enemyHit.play();
				hp -= e.damage;
				e.remove();
			}
			if(hp <= 0)
			{
				solid = false;
				dead = true;
				vx *= -1;
				vy = -20;
			}
		}
		else
		{
			vy +=2;
			if(y > Game.GAME_HEIGHT + 100)
				removeSelf();
		}
		
	}

}
