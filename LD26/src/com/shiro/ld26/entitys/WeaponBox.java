package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class WeaponBox extends Mob
{

	public WeaponBox(int x, int y, int width, int height)
	{
		super(x, y, width, height);
		type = "box";
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
		g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
		g.setColor(new Color(0.7f, 0.3f, 0.1f));
		g.fillRect(0, 0, width, height);
		g.setTransform(tx);
		super.render(g, d);
	}
	@Override
	public void update()
	{
		super.update();
		vy++;
		if(vy > Mob.MAX_FALL_SPEED)
			vy = Mob.MAX_FALL_SPEED;
		
		checkCollisions();
	}
}
