package com.shiro.ld26;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import com.shiro.ld26.screen.Screen;
import com.shiro.ld26.screen.StartScreen;

public class Game extends Canvas implements Runnable
{
	private static final long serialVersionUID = 1L;

	private final int TICKS_PER_SECOND = 30;
	private final int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
	private final int MAX_FRAMESKIP = 5;

	private final int MAX_FRAME_RATE = 120;
	private final int FRAME_CAP = 1000 / MAX_FRAME_RATE;

	public static Boolean debug = false;
	public static Boolean aa = true;
	public static Boolean smooth = true;

	public static final int GAME_WIDTH = 700;
	public static final int GAME_HEIGHT = 500;

	private Boolean running;
	private Thread t;

	private double next_game_tick = 0;
	private int loops = 0;
	private double delta = 0;

//	private int fps = 0;
//	private int tick = 0;
//	private Font font = new Font(Font.DIALOG, Font.BOLD, 14);

	private BufferStrategy bs;
	
	public static Input input = new Input();
	public static Screen currentScreen;

	public Game()
	{
		this.setPreferredSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
	}
	public static void setScreen(Screen s)
	{
		if(currentScreen != null)
			currentScreen.clear();
		currentScreen = s;
	}
	public void init()
	{
		//System.setProperty("sun.java2d.opengl","True");
		this.createBufferStrategy(2);
		bs = this.getBufferStrategy();
	}
	public void start()
	{
		this.addKeyListener(input);
		setScreen(new StartScreen());
		running = true;
		t = new Thread(this);
		t.start();
	}
	public void stop()
	{
		running = false;
	}
	private void update()
	{
		currentScreen.update();
		input.update();
	}
	private void render()
	{
		Graphics2D g = null;

		try
		{
			g = (Graphics2D) bs.getDrawGraphics();
			if(aa)
			{
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			}

			g.clearRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);

			if(!smooth)
				delta = 0;
			
			//Render stuff
			currentScreen.render(g, delta);
		}
		catch(IllegalStateException e)
		{
			
		}
		finally
		{
			if(g != null)
				g.dispose();
		}
		if(bs != null)
			bs.show();
		
		Toolkit.getDefaultToolkit().sync();
	}
	@Override
	public void run()
	{
		int frames = 0;
		int ticks = 0;
		long lastTimer1 = System.currentTimeMillis();
		next_game_tick = System.currentTimeMillis();
		while(running)
		{
			try
			{
				Thread.sleep(FRAME_CAP);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
			loops = 0;
			while(System.currentTimeMillis() > next_game_tick && loops < MAX_FRAMESKIP)
			{
				update();
				next_game_tick += SKIP_TICKS;
				loops++;
				ticks++;
			}
			delta = (System.currentTimeMillis() + SKIP_TICKS - next_game_tick) / SKIP_TICKS;
			render();

			frames++;
			if(System.currentTimeMillis() - lastTimer1 > 1000)
			{
				lastTimer1 += 1000;
//				fps = frames;
//				tick = ticks;
				System.out.println("Fps:" + frames + "  Ticks:" + ticks);
				frames = 0;
				ticks = 0;
			}
		}
	}
	public static void main(String[] args)
	{
		new Thread()
		{
			{
				setDaemon(true);
				start();
			}

			@Override
			public void run()
			{
				while(true)
				{
					try
					{
						Thread.sleep(Long.MAX_VALUE);
					}
					catch(Exception exc)
					{
					}
				}
			}
		};
		
		Game game = new Game();
		game.setSize(GAME_WIDTH, GAME_HEIGHT);
		JFrame frame = new JFrame("LD26");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(game, BorderLayout.CENTER);
		// 
		frame.setSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
		frame.setMaximumSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
		frame.setMinimumSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
		frame.pack();
		//frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		game.init();
		game.start();
	}
}
