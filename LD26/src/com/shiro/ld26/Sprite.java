package com.shiro.ld26;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Sprite
{
	public static float[] scales = { 1f, 1f, 1f, 0.5f };
	public static float[] offsets = new float[4];
	public static RescaleOp rop = new RescaleOp(scales, offsets, null);

	public static BufferedImage load(String name)
	{
		try
		{
			BufferedImage org = ImageIO.read(Sprite.class.getResource(name));
			BufferedImage res = new BufferedImage(org.getWidth(), org.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics g = res.getGraphics();
			g.drawImage(org, 0, 0, null, null);
			g.dispose();
			return res;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static BufferedImage[] splitImage(BufferedImage img, int num, int size)
	{
		BufferedImage bi[] = new BufferedImage[num];
		for(int i = 0; i < num; i++)
		{
			bi[i] = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
			Graphics g = bi[i].getGraphics();
			g.drawImage(img, -i * size, 0, null);
			g.dispose();
		}
		return bi;
	}

	public static BufferedImage[][] splitImage(BufferedImage img, int xnum, int ynum, int size)
	{
		BufferedImage bi[][] = new BufferedImage[ynum][xnum];
		for(int i = 0; i < ynum; i++)
		{
			for(int j = 0; j < xnum; j++)
			{
				bi[i][j] = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
				Graphics g = bi[i][j].getGraphics();
				g.drawImage(img, -j * size, -i * size, null);
				g.dispose();
			}

		}
		return bi;
	}

	public static BufferedImage getImage(BufferedImage img, int size, int pos)
	{
		BufferedImage i = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		Graphics g = i.getGraphics();
		g.drawImage(img, -pos * size, 0, null);
		g.dispose();
		return i;
	}
}
