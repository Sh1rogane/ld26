package com.shiro.ld26;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound
{
	public static final Sound enemyHit = new Sound("/enemyHurt.wav", false);
	public static final Sound explosion = new Sound("/Explosion.wav", false);
	public static final Sound fShoot = new Sound("/FShoot.wav", false);
	public static final Sound bShoot = new Sound("/BShoot.wav", false);
	public static final Sound pShoot = new Sound("/PShoot.wav", false);
	
	public static final Sound music = new Sound("/music.wav", true);

	private AudioClip clip;
	private boolean loop;

	public Sound(String name, boolean loop)
	{
		try
		{
			this.loop = loop;
			clip = Applet.newAudioClip(Sound.class.getResource(name));
		}
		catch(Throwable e)
		{
			e.printStackTrace();
		}
	}

	public void play()
	{
		try
		{
			new Thread()
			{
				@Override
				public void run()
				{
					if(loop)
						clip.loop();
					else
						clip.play();
				}
			}.start();
		}
		catch(Throwable e)
		{
		}
	}
	public void stop()
	{
		clip.stop();
	}
}
