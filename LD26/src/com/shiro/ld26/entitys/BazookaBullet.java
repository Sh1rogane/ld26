package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import com.shiro.ld26.Sound;

public class BazookaBullet extends Bullet
{
	private int speed = 10;

	private boolean explode;
	
	private int growthSpeed = 30;

	public BazookaBullet(int x, int y, int dir)
	{
		super(x, y, 15, 15, dir);
		this.dir = dir;
		damage = 15;
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		//super.render(g, d);
		AffineTransform tx = g.getTransform();
		g.setColor(Color.WHITE);
		g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
		if(!explode)
			g.fillRect(0, 0, width, height);
		else
			g.fillOval(0, 0, width, height);
		g.setTransform(tx);
		
	}
	@Override
	public void update()
	{
		super.update();

		if(!explode)
		{
			if(dir == Mob.RIGHT)
				vx = speed;
			else if(dir == Mob.LEFT)
				vx = -speed;
			else if(dir == Mob.UP)
				vy = -speed;
			else if(dir == Mob.DOWN)
				vy = speed;
		}

		if(width < 10)
			removeSelf();
		
		else if(explode)
		{
			vx = 0;
			vy = 0;
			width += growthSpeed;
			height += growthSpeed;
			x -= growthSpeed / 2;
			y -= growthSpeed / 2;
			if(width > 250)
				growthSpeed *= -2;
		}
	}
	@Override
	public void remove()
	{
		if(!explode)
		{
			Sound.explosion.play();
			explode = true;
		}
		
	}

}
