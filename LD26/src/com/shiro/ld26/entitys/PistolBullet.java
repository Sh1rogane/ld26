package com.shiro.ld26.entitys;

public class PistolBullet extends Bullet
{

	public PistolBullet(int x, int y, int dir)
	{
		super(x, y, 10, 10, dir);
		speed = 20;
		damage = 3;
	}

}
