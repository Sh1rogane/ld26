package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import com.shiro.ld26.Game;

public abstract class Entity
{	
	public int renderOrder = 0;
	
	protected int x;
	protected int y;

	protected int width;
	protected int height;

	protected double vx;
	protected double vy;

	protected String type = "";

	protected boolean solid = true;
	
	protected int dir;
	
	public int damage;

	public Entity(int x, int y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	public void render(Graphics2D g, double d)
	{
		g.setColor(Color.GREEN);
		//g.draw(getPredicdetHitbox());
	}
	public void update()
	{
		this.x += vx;
		this.y += vy;
	}
	public Rectangle getHitbox()
	{
		return new Rectangle(x, y, width, height);
	}
	public Rectangle getPredicdetHitbox()
	{
		return new Rectangle(x + (int)vx, y + (int)vy, width, height);
	}
	protected double getInterpolatedX(double speed, double delta)
	{
		return x + (speed * delta);
	}
	protected double getInterpolatedY(double speed, double delta)
	{
		return y + (speed * delta);
	}
	public Entity collides(String type)
	{
		ArrayList<Entity> l = Game.currentScreen.entityList;
		for(Entity e : l)
		{
			if(e.solid && e.type.equals(type))
			{
				if(getPredicdetHitbox().intersects(e.getPredicdetHitbox()))
					return e;
			}
		}
		return null;
	}
	public Entity collides(String type, double xSpeed, double ySpeed)
	{
		ArrayList<Entity> l = Game.currentScreen.entityList;
		for(Entity e : l)
		{
			if(e.solid && e.type.equals(type))
			{
				Rectangle r = e.getHitbox();
				if(getHitbox().intersects(r.x - xSpeed, r.y - ySpeed, r.width, r.height))
					return e;
			}
		}
		return null;
	}
	public Boolean collidesBoolean(String type, double xSpeed, double ySpeed)
	{
		ArrayList<Entity> l = Game.currentScreen.entityList;
		for(Entity e : l)
		{
			if(e.solid && e.type.equals(type))
			{
				Rectangle r = e.getHitbox();
				if(getHitbox().intersects(r.x - xSpeed, r.y - ySpeed, r.width, r.height))
					return true;
			}
		}
		return false;
	}
	public void remove()
	{
		removeSelf();
	}
	public void removeSelf()
	{
		Game.currentScreen.remove(this);
	}
	// protected double getInterpolatedRotation(double speed, double delta)
	// {
	// //return rotation + (speed * delta);
	// }
}
