package com.shiro.ld26.screen;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;

import com.shiro.ld26.RenderSorter;
import com.shiro.ld26.entitys.Entity;
import com.shiro.ld26.entitys.Player;

public class Screen
{
	public ArrayList<Entity> entityList = new ArrayList<Entity>();
	private ArrayList<Entity> removeList = new ArrayList<Entity>();
	private ArrayList<Entity> addList = new ArrayList<Entity>();
	private boolean sort;
	
	protected Player p;
	
	public Screen()
	{
		
	}
	public void clear()
	{
		entityList.clear();
	}
	public void add(Entity e)
	{
		sort = true;
		addList.add(e);
	}
	public void remove(Entity e)
	{
		removeList.add(e);
	}
	public void update()
	{
		for(int i = 0; i < entityList.size(); i++)
			entityList.get(i).update();
		
		entityList.removeAll(removeList);
		removeList.clear();
		entityList.addAll(addList);
		addList.clear();
		
		if(sort)
		{
			sort = false;
			Collections.sort(entityList, new RenderSorter());
		}
	}
	public void render(Graphics2D g, double delta)
	{
		for(Entity e : entityList)
		{
			e.render(g, delta);
		}
	}
	public Player getPlayer()
	{
		return p;
	}
}
