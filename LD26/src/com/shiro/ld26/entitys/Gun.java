package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;

public class Gun
{
	private Entity holder;
	
	public Gun(Entity holder)
	{
		this.holder = holder;
	}
	public void render(Graphics2D g, double d)
	{
		g.setColor(Color.WHITE);
		if(holder.dir == Mob.RIGHT)
		{
			g.fillRect(holder.width + 5, 10, 20, 5);
			g.fillRect(holder.width + 5, 15, 5, 10);
		}
		else if(holder.dir == Mob.LEFT)
		{
			g.fillRect(-25, 10, 20, 5);
			g.fillRect(-10, 15, 5, 10);
		}
		else if(holder.dir == Mob.UP)
		{
			g.fillRect(3, -10, 15, 5);
			g.fillRect(13, -25, 5, 15);
		}
		else if(holder.dir == Mob.DOWN)
		{
			g.fillRect(3, 15, 13, 5);
			g.fillRect(3, 15, 5, 20);
		}
	}
}
