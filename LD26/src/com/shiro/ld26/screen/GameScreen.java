package com.shiro.ld26.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.Random;

import com.shiro.ld26.Game;
import com.shiro.ld26.Sound;
import com.shiro.ld26.entitys.BasicEnemy;
import com.shiro.ld26.entitys.BigEnemy;
import com.shiro.ld26.entitys.Block;
import com.shiro.ld26.entitys.FlyingEnemy;
import com.shiro.ld26.entitys.Mob;
import com.shiro.ld26.entitys.Pipe;
import com.shiro.ld26.entitys.Player;
import com.shiro.ld26.entitys.TurretEnemy;
import com.shiro.ld26.entitys.WeaponBox;

public class GameScreen extends Screen
{
	private int enemyTick = 0;
	private int boxTick = 0;
	private int timeTick = 0;
	private Font font = new Font("Fixedsys Regular", Font.BOLD, 14);
	private boolean boss;
	
	private TurretEnemy t1 = new TurretEnemy(130, 460, Mob.UP);
	private TurretEnemy t2 = new TurretEnemy(330, 460, Mob.UP);
	private TurretEnemy t3 = new TurretEnemy(530, 460, Mob.UP);
	private TurretEnemy t4 = new TurretEnemy(320, 10, Mob.DOWN);
	
	public GameScreen()
	{
		Sound.music.play();
		p = new Player(350, 400, 20, 40);
		add(p);
		add(new Block(0, 0, Game.GAME_WIDTH, 10));
		add(new Block(0, Game.GAME_HEIGHT - 10, Game.GAME_WIDTH, 10));
		add(new Block(0, 0, 10, Game.GAME_HEIGHT));
		add(new Block(Game.GAME_WIDTH - 10, 0, 10, Game.GAME_HEIGHT));
		
		add(new Block(0, 100, 200, 30));
		add(new Block(Game.GAME_WIDTH - 200, 100, 200, 30));
		
		add(new Block(Game.GAME_WIDTH / 2 - 100, 220, 200, 30));
		
		//add(new Block(Game.GAME_WIDTH -50, 220, 50, 30));
		//add(new Block(0, 220, 50, 30));
		
		add(new Block(100, 340, 100, 30));
		add(new Block(Game.GAME_WIDTH - 200, 340, 100, 30));
		
		add(new Pipe(0, 30, 70, 60));
		add(new Pipe(Game.GAME_WIDTH - 70, 30, 70, 60));
		add(new Pipe(Game.GAME_WIDTH / 2 - 35, 0, 70, 40));
		
		
		
	}
	@Override
	public void clear()
	{
		super.clear();
		Sound.music.stop();
	}
	@Override
	public void update()
	{
		super.update();
		Random r = new Random();
		enemyTick++;
		boxTick++;
		timeTick++;
		if(timeTick < 500)
		{
			if(enemyTick > 60)
			{
				enemyTick = 0;
				
				if(r.nextBoolean())
				{
					add(new BasicEnemy(20, 30, 20, 40, Mob.RIGHT));
				}
				else
				{
					add(new BasicEnemy(Game.GAME_WIDTH - 30, 30, 20, 40, Mob.LEFT));
				}
				//add(new BigEnemy(100, 30, 40, 40, Mob.RIGHT));
			}
		}
		else if(timeTick < 1000)
		{
			if(enemyTick > 50)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					add(new BasicEnemy(20, 30, 20, 40, Mob.RIGHT));
				}
				else
				{
					add(new FlyingEnemy(Game.GAME_WIDTH / 2 - 30, 10, 30, 20));
					add(new BasicEnemy(Game.GAME_WIDTH - 30, 30, 20, 40, Mob.LEFT));
				}
			}
		}
		else if(timeTick < 1500)
		{
			if(enemyTick > 50)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					if(r.nextBoolean())
						add(new BasicEnemy(20, 30, 20, 40, Mob.RIGHT));
					else
						add(new BigEnemy(20, 30, 40, 40, Mob.RIGHT));
					
				}
				else
				{
					if(r.nextBoolean())
						add(new BasicEnemy(Game.GAME_WIDTH - 30, 30, 20, 40, Mob.LEFT));
					else
						add(new BigEnemy(Game.GAME_WIDTH - 60, 30, 40, 40, Mob.LEFT));
					
				}
				add(new FlyingEnemy(Game.GAME_WIDTH / 2 - 30, 10, 30, 20));
			}
		}
		else if(timeTick < 2000)
		{
			if(enemyTick > 40)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					if(r.nextBoolean())
						add(new BasicEnemy(20, 30, 20, 40, Mob.RIGHT));
					else
						add(new BigEnemy(20, 30, 40, 40, Mob.RIGHT));
					
				}
				else
				{
					if(r.nextBoolean())
						add(new BasicEnemy(Game.GAME_WIDTH - 30, 30, 20, 40, Mob.LEFT));
					else
						add(new BigEnemy(Game.GAME_WIDTH - 60, 30, 40, 40, Mob.LEFT));
					
				}
				add(new FlyingEnemy(Game.GAME_WIDTH / 2 - 30, 10, 30, 20));
			}
		}
		else if(timeTick < 2500)
		{
			if(enemyTick > 40)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					add(new BigEnemy(20, 30, 40, 40, Mob.RIGHT));
				}
				else
				{
					add(new BigEnemy(Game.GAME_WIDTH - 60, 30, 40, 40, Mob.LEFT));
				}
			}
		}
		else if(timeTick < 3000)
		{
			if(enemyTick > 30)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					add(new BasicEnemy(Game.GAME_WIDTH - 30, 30, 20, 40, Mob.LEFT));
				}
				else
				{
					add(new BasicEnemy(20, 30, 20, 40, Mob.RIGHT));
				}
				add(new FlyingEnemy(Game.GAME_WIDTH / 2 - 30, 10, 30, 20));
			}
		}
		else if(timeTick < 3500)
		{
			if(enemyTick > 40)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					if(r.nextBoolean())
						add(new BasicEnemy(20, 30, 20, 40, Mob.RIGHT));
					else
						add(new BigEnemy(20, 30, 40, 40, Mob.RIGHT));
				}
				else
				{
					if(r.nextBoolean())
						add(new BasicEnemy(Game.GAME_WIDTH - 30, 30, 20, 40, Mob.LEFT));
					else
						add(new BigEnemy(Game.GAME_WIDTH - 60, 30, 40, 40, Mob.LEFT));
				}
			}
		}
		else if(timeTick < 4000)
		{
			if(enemyTick > 40)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					if(r.nextBoolean())
						add(new BasicEnemy(20, 30, 20, 40, Mob.RIGHT));
					else
						add(new BigEnemy(20, 30, 40, 40, Mob.RIGHT));
				}
				else
				{
					if(r.nextBoolean())
						add(new BasicEnemy(Game.GAME_WIDTH - 30, 30, 20, 40, Mob.LEFT));
					else
						add(new BigEnemy(Game.GAME_WIDTH - 60, 30, 40, 40, Mob.LEFT));
				}
				add(new FlyingEnemy(Game.GAME_WIDTH / 2 - 30, 10, 30, 20));
			}
		}
		else if(timeTick < 5400)
		{
			if(enemyTick > 40)
			{
				enemyTick = 0;
				if(r.nextBoolean())
				{
					add(new BigEnemy(20, 30, 40, 40, Mob.RIGHT));
				}
				else
				{
					add(new BigEnemy(Game.GAME_WIDTH - 60, 30, 40, 40, Mob.LEFT));
				}
			}
		}
		else
		{
			if(!boss)
			{
				boss = true;
				add(t1);
				add(t2);
				add(t3);
				add(t4);
				return;
			}
		}
		if(boss)
		{
			if(!entityList.contains(t1) && !entityList.contains(t2) && !entityList.contains(t3) && !entityList.contains(t4))
			{
				Game.setScreen(new WinScreen());
			}
		}
		if(boxTick >= 300)
		{
			boxTick = 0;
			int t = r.nextInt(6);
			if(t == 0)
				add(new WeaponBox(320, 100, 30, 30));
			else if(t == 1)
				add(new WeaponBox(320, 300, 30, 30));
			else if(t == 2)
				add(new WeaponBox(520, 200, 30, 30));
			else if(t == 3)
				add(new WeaponBox(120, 200, 30, 30));
			else if(t == 4)
				add(new WeaponBox(120, 50, 30, 30));
			else if(t == 5)
				add(new WeaponBox(520, 50, 30, 30));
		}
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		super.render(g, d);
		drawHud(g);
	}
	private void drawHud(Graphics2D g)
	{
		g.setColor(Color.GREEN);
		g.setFont(font);
		g.drawString("Weapon: " + p.weapon, 15, 25);
		g.drawString("Ammo: " + p.ammo, 15, 45);
		g.drawString("Time: " + timeTick, 15, 65);
	}
}
