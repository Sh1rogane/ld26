package com.shiro.ld26.screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import com.shiro.ld26.Game;
import com.shiro.ld26.Input;

public class GameOverScreen extends Screen
{
	private Font font1 = new Font("Fixedsys Regular", Font.BOLD, 40);
	private Font font2 = new Font("Fixedsys Regular", Font.BOLD, 30);
	
	public GameOverScreen()
	{
	}
	@Override
	public void update()
	{
		super.update();
		if(Input.isKeyTyped(KeyEvent.VK_ENTER))
			Game.setScreen(new GameScreen());
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		super.render(g, d);
		drawHud(g);
	}
	private void drawHud(Graphics2D g)
	{
		g.setColor(Color.WHITE);
		g.setFont(font1);
		g.drawString("YOU DIED!", 250, 100);
		g.setFont(font2);
		g.drawString("Press Enter To Start", 200, 300);
	}
}
