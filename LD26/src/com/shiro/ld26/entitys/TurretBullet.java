package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class TurretBullet extends Bullet
{

	public TurretBullet(int x, int y, double bvx, double bvy)
	{
		super(x, y, 10, 10, 1);
		this.vx = bvx;
		this.vy = bvy;
		type = "enemy";
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
        g.setColor(Color.CYAN);
        g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
        g.fillRect(0, 0, width, height);
        g.setTransform(tx);
	}
	@Override
	public void update()
	{
		this.x += vx;
		this.y += vy;
		
		if(collides("block") != null)
			remove();
	}
}
