package com.shiro.ld26;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Dimension;

public class GameApplet extends Applet
{
	private static final long serialVersionUID = 1L;
	
	private Game game;
	
	public GameApplet()
	{
		
	}
	@Override
    public void init()
    {
		this.setLayout(new BorderLayout());
		game = new Game();
		this.add(game, BorderLayout.CENTER);
		this.setSize(new Dimension(Game.GAME_WIDTH, Game.GAME_HEIGHT));
		this.setMinimumSize(new Dimension(Game.GAME_WIDTH, Game.GAME_HEIGHT));
		this.setMaximumSize(new Dimension(Game.GAME_WIDTH, Game.GAME_HEIGHT));
		game.init();
    }
	@Override
	public void start()
	{
		game.start();
	}
	@Override
	public void stop()
	{
		game.stop();
		Sound.music.stop();
	}
	
}
