package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;

public class Pipe extends Entity
{

	public Pipe(int x, int y, int width, int height)
	{
		super(x, y, width, height);
		renderOrder = -1;
		solid = false;
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		g.setColor(new Color(0f,0.3f,0f));
		g.fillRect(x, y, width, height);
	}
}
