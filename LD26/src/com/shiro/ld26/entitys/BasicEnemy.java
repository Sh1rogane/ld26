package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import com.shiro.ld26.Game;
import com.shiro.ld26.Sound;

public class BasicEnemy extends Mob
{

	private int hurtTick = 0;
	public BasicEnemy(int x, int y, int width, int height, int dir)
	{
		super(x, y, width, height);
		this.dir = dir;
		speed = 5;
		if(dir == Mob.RIGHT)
			vx = speed;
		if(dir == Mob.LEFT)
			vx = -speed;
		
		renderOrder = 1;
		hp = 3;
		type = "enemy";
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
        g.setColor(Color.RED);
        if(hurt)
        	g.setColor(Color.WHITE);
        g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
        g.fillRect(0, 0, width, height);
        g.setTransform(tx);
        super.render(g, d);
	}
	@Override
	public void update()
	{
		super.update();
		
		if(!dead)
		{
			if(hurt)
			{
				hurtTick ++;
				if(hurtTick > 5)
					hurt = false;
			}
			
			vy++;
			if(vy >= Mob.MAX_FALL_SPEED)
				vy = Mob.MAX_FALL_SPEED;
			
			Entity e = collides("bullet");
			if(e != null)
			{
				hurt = true;
				Sound.enemyHit.play();
				hp -= e.damage;
				e.remove();
			}
			if(hp <= 0)
			{
				solid = false;
				dead = true;
				vx *= -1;
				vy = -15;
			}
			checkCollisions();
		}
		else
		{
			vy +=2;
			if(y > Game.GAME_HEIGHT + 100)
				removeSelf();
		}
	}
	@Override
	protected void checkCollisions()
	{
		Entity e = collides("block", 0, vy);
		if(e != null)
		{
			if(vy > 0)
			{
				inAir = false;
				vy = 0;
				y = e.getHitbox().y - height;
			}
			else if(vy < 0)
			{
				vy = 0;
				y = (int) e.getHitbox().getMaxY();
			}
		}
		e = collides("block", vx, 0);
		if(e != null)
		{
			if(vx > 0)
			{
				vx *= -1;
				x = e.getHitbox().x - width;
			}
			else if(vx < 0)
			{
				vx *= -1;
				x = (int) e.getHitbox().getMaxX();
			}
		}
	}
}
