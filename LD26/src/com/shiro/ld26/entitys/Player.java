package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.util.Random;

import com.shiro.ld26.Game;
import com.shiro.ld26.Input;
import com.shiro.ld26.Sound;
import com.shiro.ld26.screen.GameOverScreen;

public class Player extends Entity
{
	public static final String PISTOL = "Pistol";
	public static final String FLAMETHROWER = "Flamethrower";
	public static final String BAZOOKA = "Bazooka";
	
	
	private int speed = 8;
	private int tempDir = 1;
	private boolean inAir;

	private boolean dead;

	private Gun gun = new Gun(this);
	
	public String weapon = PISTOL;
	public int ammo = 99;
	
	private int fireTick = 100;
	
	public Player(int x, int y, int width, int height)
	{
		super(x, y, width, height);
		type = "player";
		dir = Mob.RIGHT;
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
		g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, width, height);
		gun.render(g, d);
		g.setTransform(tx);
		super.render(g, d);
	}
	@Override
	public void update()
	{
		super.update();
		if(!dead)
		{
			if(Input.isKeyDown(KeyEvent.VK_RIGHT))
			{
				vx = speed;
				dir = Mob.RIGHT;
				tempDir = Mob.RIGHT;
			}
			else if(Input.isKeyDown(KeyEvent.VK_LEFT))
			{
				vx = -speed;
				dir = Mob.LEFT;
				tempDir = Mob.LEFT;
			}
			else
			{
				dir = tempDir;
				vx = 0;
			}
			if(Input.isKeyDown(KeyEvent.VK_UP))
			{
				dir = Mob.UP;
			}
			else if(Input.isKeyDown(KeyEvent.VK_DOWN))
			{
				dir = Mob.DOWN;
			}
			else
			{
				dir = tempDir;
			}
			vy++;
			if(vy >= Mob.MAX_FALL_SPEED)
				vy = Mob.MAX_FALL_SPEED;

			if(Input.isKeyTyped(KeyEvent.VK_Z) && !inAir)
			{
				inAir = true;
				vy = -17;
			}
			fireTick++;
			if(Input.isKeyDown(KeyEvent.VK_X))
			{
				if(weapon.equals(PISTOL) && fireTick >= 10)
				{
					fireTick = 0;
					Sound.pShoot.play();
					if(dir == Mob.RIGHT)
						Game.currentScreen.add(new PistolBullet(x + width + 25, y + 8, dir));
					else if(dir == Mob.LEFT)
						Game.currentScreen.add(new PistolBullet(x - 40, y + 8, dir));
					else if(dir == Mob.UP)
						Game.currentScreen.add(new PistolBullet(x + 10, y - 30, dir));
					else if(dir == Mob.DOWN)
						Game.currentScreen.add(new PistolBullet(x, y + 30, dir));
				}
				else if(weapon.equals(FLAMETHROWER))
				{
					ammo--;
					Sound.fShoot.play();
					if(dir == Mob.RIGHT)
						Game.currentScreen.add(new MBullet(x + width + 25, y + 8, dir));
					else if(dir == Mob.LEFT)
						Game.currentScreen.add(new MBullet(x - 40, y + 8, dir));
					else if(dir == Mob.UP)
						Game.currentScreen.add(new MBullet(x + 10, y - 30, dir));
					else if(dir == Mob.DOWN)
						Game.currentScreen.add(new MBullet(x, y + 30, dir));
					
					if(ammo <= 0)
					{
						ammo = 99;
						weapon = PISTOL;
					}
				}
				else if(weapon.equals(BAZOOKA) && fireTick >= 30)
				{
					fireTick = 0;
					ammo--;
					Sound.bShoot.play();
					if(dir == Mob.RIGHT)
						Game.currentScreen.add(new BazookaBullet(x + width + 25, y + 8, dir));
					else if(dir == Mob.LEFT)
						Game.currentScreen.add(new BazookaBullet(x - 40, y + 8, dir));
					else if(dir == Mob.UP)
						Game.currentScreen.add(new BazookaBullet(x + 10, y - 30, dir));
					else if(dir == Mob.DOWN)
						Game.currentScreen.add(new BazookaBullet(x, y + 30, dir));
					if(ammo <= 0)
					{
						ammo = 99;
						weapon = PISTOL;
					}
				}
			}
			checkCollisions();

			if(collides("enemy") != null)
			{
				vy = -30;
				dead = true;
				Sound.music.stop();
			}
			
			Entity e = collides("box");
			if(e != null)
			{
				e.remove();
				Random r = new Random();
				
				if(r.nextBoolean())
				{
					ammo = 200;
					weapon = FLAMETHROWER;
				}
				else
				{
					ammo = 5;
					weapon = BAZOOKA;
				}
			}
		}
		else
		{
			vy +=2;
			if(y > Game.GAME_HEIGHT + 100)
				Game.setScreen(new GameOverScreen());
		}
	}
	private void checkCollisions()
	{
		Entity e = collides("block", 0, vy);
		if(e != null)
		{
			if(vy > 0)
			{
				inAir = false;
				vy = 0;
				y = e.getHitbox().y - height;
			}
			else if(vy < 0)
			{
				vy = 0;
				y = (int) e.getHitbox().getMaxY();
			}
		}
		e = collides("block", vx, 0);
		if(e != null)
		{
			if(vx > 0)
			{
				vx = 0;
				x = e.getHitbox().x - width;
			}
			else if(vx < 0)
			{
				vx = 0;
				x = (int) e.getHitbox().getMaxX();
			}
		}
	}
}
