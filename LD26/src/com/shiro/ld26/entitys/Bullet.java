package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Bullet extends Entity
{
	protected int speed = 20;
	
	public Bullet(int x, int y, int width, int height, int dir)
	{
		super(x, y, width, height);
		this.dir = dir;
		type = "bullet";
		renderOrder = 2;
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		AffineTransform tx = g.getTransform();
        g.setColor(Color.WHITE);
        g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
        g.fillRect(0, 0, width, height);
        g.setTransform(tx);
        super.render(g, d);
	}
	@Override
	public void update()
	{
		super.update();
		
		if(dir == Mob.RIGHT)
			vx = speed;
		else if(dir == Mob.LEFT)
			vx = -speed;
		else if(dir == Mob.UP)
			vy = -speed;
		else if(dir == Mob.DOWN)
			vy = speed;
		
//		if(x < 0 || x > Game.GAME_WIDTH || y < 0 || y > Game.GAME_HEIGHT)
//			removeSelf();
		if(collides("block") != null)
			remove();
	}

}
