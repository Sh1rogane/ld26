package com.shiro.ld26.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.Random;

public class MBullet extends Bullet
{
	private Random r = new Random();
	private double rX;
	private double rY;
	private int removeTick = 0;
	
	public MBullet(int x, int y, int dir)
	{
		super(x, y, 5, 5, dir);
		speed = 20;
		rX = r.nextInt(4) - 2;
		rY = r.nextInt(4) - 2;
		damage = 1;
	}
	@Override
	public void render(Graphics2D g, double d)
	{
		super.render(g, d);
		AffineTransform tx = g.getTransform();
        g.setColor(Color.YELLOW);
        g.translate(getInterpolatedX(vx, d), getInterpolatedY(vy, d));
        g.fillRect(0, 0, width, height);
        g.setTransform(tx);
	}
	@Override
	public void update()
	{
		super.update();
		vx += rX;
		vy += rY;
		
		removeTick++;
		if(removeTick > 7)
			remove();
	}
}
