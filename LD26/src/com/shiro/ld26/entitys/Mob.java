package com.shiro.ld26.entitys;

public class Mob extends Entity
{
	public static final int RIGHT = 1;
	public static final int DOWN = 2;
	public static final int LEFT = 3;
	public static final int UP = 4;
	public static final int MAX_FALL_SPEED = 16;
	
	protected boolean dead;
	protected int speed;
	protected boolean inAir;
	
	public int hp;
	
	protected boolean hurt;
	
	public Mob(int x, int y, int width, int height)
	{
		super(x, y, width, height);
	}
	protected void checkCollisions()
	{
		Entity e = collides("block", 0, vy);
		if(e != null)
		{
			if(vy > 0)
			{
				inAir = false;
				vy = 0;
				y = e.getHitbox().y - height;
			}
			else if(vy < 0)
			{
				vy = 0;
				y = (int) e.getHitbox().getMaxY();
			}
		}
		e = collides("block", vx, 0);
		if(e != null)
		{
			if(vx > 0)
			{
				vx = 0;
				x = e.getHitbox().x - width;
			}
			else if(vx < 0)
			{
				vx = 0;
				x = (int) e.getHitbox().getMaxX();
			}
		}
	}

}
