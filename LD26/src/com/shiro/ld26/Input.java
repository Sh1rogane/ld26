package com.shiro.ld26;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Map;

public class Input implements MouseListener, MouseMotionListener, KeyListener
{
	
	private static Map<Integer, Boolean> keyDown = new HashMap<Integer, Boolean>();
	private static Map<Integer, Boolean> keyTyped = new HashMap<Integer, Boolean>();

	
	
	public static boolean isKeyDown(int keyCode)
	{
		Boolean b = keyDown.get(keyCode);
		if(b != null)
			return b;
		else
			return false;
	}
	public static boolean isKeyTyped(int keyCode)
	{
		Boolean b = keyTyped.get(keyCode);
		if(b != null)
			return b;
		else
			return false;
	}
	public void update()
	{
		keyTyped.clear();
	}
	@Override
	public void keyPressed(KeyEvent e)
	{
		Boolean b1 = isKeyDown(e.getKeyCode());
		if(b1 != null && b1 == false)
			keyTyped.put(e.getKeyCode(), true);	
		
		keyDown.put(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		keyDown.put(e.getKeyCode(), false);
		keyTyped.put(e.getKeyCode(), false);
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
	}
	
}
